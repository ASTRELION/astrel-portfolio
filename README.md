# Astrel Portfolio

My custom portfolio website.

Uses the following technologies:

- [TypeScript][typescript]
- [Svelte][svelte]
- [SvelteKit][sveltekit]
- [Bun][bun]
- [TailwindCSS][tailwind]
- [FiraCode Font][firacode]
- [Font Awesome][fontawesome]

## Development

*The following commands are for [Bun][bun], but you could reasonably replace them with their [NPM][npm] or [Yarn][yarn] equivalents. Though, if you intend on contributing, you should use [Bun][bun].*

### Dev

1. `bun install`
2. `bun --bun run dev`
3. Navigate to `http://localhost:5173`

### Production

1. `bun install`
2. `bun run build`
3. `cd build`
4. `bun run start`
5. Navigate to `http://localhost:3000`

### Docker

1. `bun install`
2. `docker build . -t astrel-portfolio` or use the [prebuilt package](https://gitlab.com/ASTRELION/astrel-portfolio/-/packages)
3. Use the [provided docker-compose.yml](./docker-compose.yml) and run `docker-compose up -d`
    - Set `image` to `docker.io/library/astrel-portfolio` for local builds
    - Set `image` to `registry.gitlab.com/astrelion/astrel-portfolio:latest` to use the prebuilt image
4. Navigate to `http://localhost:3000`



[bun]: https://bun.sh
[firacode]: https://github.com/tonsky/FiraCode
[fontawesome]: https://fontawesome.com
[npm]: https://www.npmjs.com
[svelte]: https://svelte.dev
[sveltekit]: https://kit.svelte.dev
[tailwind]: https://tailwindcss.com
[typescript]: https://www.typescriptlang.org
[yarn]: https://yarnpkg.com
