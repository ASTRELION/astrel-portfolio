import type { Config } from "drizzle-kit";

export default {
    schema: "./src/lib/db/schema.ts",
    out: "./src/lib/db",
    driver: "pg",
    dbCredentials: {
        connectionString: String(process.env.DB_URL),
        ssl: true,
    },
} satisfies Config;
