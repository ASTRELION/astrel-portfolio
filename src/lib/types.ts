export type Project =
{
    name: string,
    description: string,
    tags: string[],
    url: string,
    stars: number,
    forks: number,
    createdAt: Date,
    updatedAt: Date,
    isFork: boolean,
    image: string | null,
    featured: number | null,
    altTitle: string | null,
    altDescription: string | null,
    altImage: string | null,
    platform: "gitlab" | "github",
}

export type LinkItem =
{
    name?: string,
    url?: string,
    icon?: string,
    extras?:
    {
        icon: string,
        hover: string,
        url: string,
    }[],
    items?: LinkItem[],
    tag?: string,
};

export interface PageMeta
{
    title?: string;
    description?: string;
    author?: string,
    keywords?: string;
    type?: string;
    image?: string;
    imageWidth?: number;
    imageHeight?: number;
    url?: string;
}
