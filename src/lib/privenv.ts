import { env } from "$env/dynamic/private";

/** TTL for redis entries in seconds. Defaults to 1 DAY. */
export const REDIS_TTL = env.REDIS_TTL ? parseInt(env.REDIS_TTL) : 60 * 60 * 24;
