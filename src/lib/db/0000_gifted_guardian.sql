CREATE TABLE IF NOT EXISTS "kofi" (
	"uuid" serial PRIMARY KEY NOT NULL,
	"created_at" timestamp DEFAULT now(),
	"name" text,
	"message" text,
	"email" text,
	"type" text,
	"public" boolean,
	"is_subscription_payments" boolean,
	"is_first_subscription_payment" boolean,
	"shop_items" json,
	"tier" text
);
