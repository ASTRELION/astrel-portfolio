import { boolean, json, pgTable, text, timestamp, uuid } from "drizzle-orm/pg-core";

export const kofi = pgTable("kofi", {
    id: uuid("id").primaryKey().defaultRandom(),
    createdAt: timestamp("created_at").defaultNow(),
    // user info
    name: text("name"),
    message: text("message"),
    email: text("email"),
    // donation info
    type: text("type"),
    public: boolean("public"),
    isSubscriptionPayment: boolean("is_subscription_payments"),
    isFirstSubscriptionPayment: boolean("is_first_subscription_payment"),
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    shopItems: json("shop_items").$type<any[]>(),
    tier: text("tier"),
});
