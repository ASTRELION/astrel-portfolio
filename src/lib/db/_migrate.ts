import { drizzle } from "drizzle-orm/postgres-js";
import { migrate } from "drizzle-orm/postgres-js/migrator";
import postgres from "postgres";

async function main(): Promise<void>
{
    console.log("Running migrations...");
    const client = postgres(process.env.DB_URL!, { max: 1, prepare: false });
    const db = drizzle(client);
    await migrate(db, { migrationsFolder: "./src/lib/db" });
    client.end();
    console.log("Successfully migrated.");
}

main();
