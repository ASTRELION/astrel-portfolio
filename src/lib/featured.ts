export type Featured =
{
    url: string,
    title?: string,
    image?: string,
    description?: string,
}

/**
 * List of featured projects.
 *
 * `description` supports Markdown, which is parsed into HTML for display on
 * the featured tab of /projects only.
 */
const featured: Featured[] = [
    {
        url: "gitlab.com/ASTRELION/exodle",
        description: "Web-based daily guessing game for Destiny 2 exotic weapons and armor. Play it [here](https://exodle.astrelion.com)."
    },
    {
        url: "gitlab.com/ASTRELION/dungeons-bot",
        description: "Made with Python and the [Mastodon API](https://github.com/halcy/Mastodon.py), this bot conducts [D&D](https://dnd.wizards.com)-inspired campaigns through public polls every half hour. It currently boasts more than 2,700 followers and has had an [article in PCGamer](https://pcgamer.com/yet-another-reason-to-ditch-musks-twitter-you-can-play-a-crowdsourced-dungeons-and-dragons-campaign-over-on-mastodon) written about it.",
    },
    {
        url: "gitlab.com/ASTRELION/gameoflife",
        description: "Made with Python and the [Mastodon API](https://github.com/halcy/Mastodon.py), this bot simulates gardens inspired by [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life), with a new post every hour. It currently has around 150 followers."
    },
    {
        url: "gitlab.com/ASTRELION/the-last-light",
        description: "This Godot 4 game was my solo entry for the [Godot Wild Jam #64](https://itch.io/jam/godot-wild-jam-64), with the main theme of Illumination and made in 1 week. It placed 13 for accessibility, 17 for theme, and 32 overall out of 118 entries."
    },
    {
        url: "gitlab.com/ASTRELION/rosemind",
        description: "This Godot 3 game was my solo entry for the [Godot Wild Jam #49](https://itch.io/jam/godot-wild-jam-49) and was my first Godot game, with the main theme of Unstable and made in 1 week. It placed 39 for graphics, 42 for originality, and 49 overall out of 71 entries."
    },
    {
        url: "gitlab.com/ASTRELION/launchme",
        description: "This is a [Minecraft](https://minecraft.net) [Bukkit](https://dev.bukkit.org)/[Spigot](https://www.spigotmc.org)/[Paper](https://papermc.io) plugin that allows players to ride on top of projectiles they shoot with deep configuration and permission options. It has a 5-star rating and has had more than 1,000 downloads so far, and is the spiritual successor to a similar plugin I also made that had more than 4,000 downloads."
    },
    {
        url: "gitlab.com/ASTRELION/GAME",
        description: "Made as a 4-person semester-long group project in university, GAME is a console-like application for [Raspberry Pi](https://www.raspberrypi.com) devices. It essentially turns any Raspberry Pi into a multiplayer-capable game console. I worked primarily alongside 1 other person in [C++](https://en.wikipedia.org/wiki/C%2B%2B) and [SFML](https://www.sfml-dev.org) for the client-side portion of the application, while 2 others worked on the server implementations written in Java."
    },
    {
        url: "gitlab.com/ASTRELION/individual437",
        description: "These are a collection of a couple games and graphics applications made for university, primarily consisting of [C#](https://learn.microsoft.com/en-us/dotnet/csharp) games written for [Unity](https://unity.com) and [Microsoft XNA](https://en.wikipedia.org/wiki/Microsoft_XNA)."
    },
    {
        url: "gitlab.com/ASTRELION/space-sequence",
        description: "Space Sequence is an online animation creation tool created as a 5-person semester-long group project in university. I acted as team lead and defined the group's direction and assisted in programming and project management. It was made primarily using [Next.js](https://nextjs.org) and [Three.js](https://threejs.org)."
    },
    {
        url: "gitlab.com/ASTRELION/advent-of-code",
        description: "These are my [Advent of Code](https://adventofcode.com) solutions. As of now, it currently has solutions for 2023 and they are all written in [Rust](https://www.rust-lang.org)."
    },
    {
        url: "gitlab.com/ASTRELION/astrel-portfolio",
        description: "This is the website you are on right now! It was made with [TypeScript](https://www.typescriptlang.org), [SvelteKit](https://kit.svelte.dev), and [Bun](https://bun.sh)."
    },
];

export default featured;
