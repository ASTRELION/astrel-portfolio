import { env } from "$env/dynamic/private";
import { Redis } from "ioredis";
import { REDIS_TTL } from "$lib/privenv";
import postgres from "postgres";
import { drizzle, type PostgresJsDatabase } from "drizzle-orm/postgres-js";

let redisInstance: Redis | null = null;
let dbInstance: PostgresJsDatabase | null = null;

export async function getDB(): Promise<PostgresJsDatabase>
{
    if (dbInstance === null)
    {
        // https://orm.drizzle.team/docs/get-started-postgresql#supabase
        const client = postgres(env.DB_URL!, { prepare: false });
        dbInstance = drizzle(client);
    }

    return dbInstance;
}

export function getRedis(): Redis | null
{
    if (redisInstance === null && env.REDIS_URL !== undefined)
    {
        redisInstance = new Redis(env.REDIS_URL!);
    }

    return redisInstance;
}

export type RedisOptions =
{
    parse?: boolean
}

export async function getRedisData(url: URL, redisOptions: RedisOptions = { parse: true }): Promise<string | null>
{
    const redis = getRedis();
    if (redis === null)
    {
        return null;
    }

    const cachedData = await redis.get(urlKey(url));

    if (redisOptions.parse && cachedData !== null)
    {
        return JSON.parse(cachedData);
    }

    return cachedData;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function setRedisData(url: URL, data: any, redisOptions: RedisOptions = { parse: true }): Promise<"" | "OK">
{
    const redis = getRedis();
    if (redis === null)
    {
        return "";
    }

    if (redisOptions.parse)
    {
        data = JSON.stringify(data);
    }

    const result = await redis.set(urlKey(url), data, "EX", REDIS_TTL);
    return result;
}

export function urlKey(url: URL): string
{
    return url.pathname;
}
