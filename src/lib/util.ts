export function normalizeName(name: string): string
{
    return name.toLowerCase().replaceAll(/[^a-zA-Z0-9]/g, "");
}
