import type { LinkItem } from "$lib/types";

const links: LinkItem[] = [
    {
        name: "Website",
        tag: "website",
        url: "https://astrelion.com",
        icon: "fa-solid fa-globe",
    },
    // If the Mastodon link changes, it needs to be manually updated in the
    // routes/+layout.svelte <noscript> tag
    {
        name: "Mastodon",
        tag: "mastodon",
        url: "https://mastodon.social/@astrelion",
        icon: "fa-brands fa-mastodon",
        extras: [
            {
                icon: "fa-brands fa-bluesky",
                url: "https://bsky.app/profile/astrelion.com",
                hover: "https://bsky.app/profile/astrelion.com",
            },
            {
                icon: "fa-brands fa-x-twitter",
                url: "https://x.com/kingASTRELION",
                hover: "https://x.com/kingASTRELION",
            },
        ],
    },
    {
        name: "Pixelfed",
        tag: "pixelfed",
        url: "https://pixelfed.social/@astrelion",
        icon: "fa-solid fa-image",
        extras: [
            {
                icon: "fa-brands fa-instagram",
                url: "https://instagram.com/astrelion",
                hover: "https://instagram.com/astrelion",
            },
        ],
    },
    {
        tag: "video",
        items: [
            {
                name: "Twitch",
                tag: "twitch",
                url: "https://twitch.tv/kingastrelion",
                icon: "fa-brands fa-twitch"
            },
            {
                name: "YouTube",
                tag: "youtube",
                url: "https://youtube.com/@astrelion",
                icon: "fa-brands fa-youtube"
            },
        ]
    },
    // If the GitLab link changes, it needs to be manually updated in the
    // routes/+layout.svelte <noscript> tag
    {
        tag: "code",
        url: "https://gitlab.com/ASTRELION",
        name: "GitLab",
        icon: "fa-brands fa-gitlab",
        extras: [
            {
                url: "https://github.com/ASTRELION",
                hover: "https://github.com/ASTRELION",
                icon: "fa-brands fa-github"
            },
            {
                url: "https://codeberg.org/ASTRELION",
                hover: "https://codeberg.org/ASTRELION",
                icon: "fa-solid fa-mountain",
            },
        ],
    },
    {
        name: "itch.io",
        tag: "itchio",
        url: "https://astrelion.itch.io",
        icon: "fa-brands fa-itch-io"
    },
    {
        tag: "donate",
        items: [
            {
                name: "Liberapay",
                tag: "liberapay",
                url: "https://liberapay.com/ASTRELION",
                icon: "fa-solid fa-credit-card"
            },
            {
                name: "Ko-fi",
                tag: "kofi",
                url: "https://ko-fi.com/astrelion",
                icon: "fa-solid fa-mug-saucer"
            },
        ]
    },
    // If the Email link changes, it needs to be manually updated in the
    // routes/+layout.svelte <noscript> tag
    {
        name: "Email",
        tag: "email",
        url: "mailto:contact@astrelion.com",
        icon: "fa-solid fa-envelope",
    }
];

export default links;
