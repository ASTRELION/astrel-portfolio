import type { PageMeta } from "$lib/types";
import type { LayoutLoadEvent } from "./$types";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export async function load({ url }: LayoutLoadEvent)
{
    return {
        meta: {
            title: "ASTRELION",
            author: "ASTRELION",
            description: "ASTRELION's projects, links, and contact information",
            keywords: "ASTRELION,portfolio,contact,about,projects,software,engineer,developer,links",
            type: "website",
            image: `${url.origin}/icon.webp`,
            imageWidth: 1024,
            imageHeight: 1024,
            url: `${url.toString().replace(/\/$/, "")}`
        } satisfies PageMeta
    };
}
