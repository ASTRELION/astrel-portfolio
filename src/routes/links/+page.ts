import type { LinkItem, PageMeta } from "$lib/types";
import type { PageLoadEvent } from "./$types";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export async function load({ fetch, parent }: PageLoadEvent)
{
    const linkPromise: Promise<LinkItem[]> = new Promise((resolve, reject) =>
    {
        fetch("/api/links")
            .then((response) =>
            {
                return response.json();
            })
            .then((json) =>
            {
                resolve(json);
            })
            .catch((error) =>
            {
                reject(error);
            });
    });

    const p = await parent();

    return {
        streamed: {
            links: linkPromise
        },
        meta: {
            ...p.meta,
            title: "ASTRELION's Links",
            description: "ASTRELION's links and contact information",
        } satisfies PageMeta
    };
}
