import { writable } from "svelte/store";

export enum Sorts
{
    ALL = "all",
    FEATURED = "featured",
    GAMES = "games",
    WEB = "web",
    BOTS = "bots",
}

export const activeTab = writable(Sorts.FEATURED);
