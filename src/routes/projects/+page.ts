import Navigation from "./Navigation.svelte";
import type { PageMeta, Project } from "$lib/types";
import type { PageLoadEvent } from "./$types";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export async function load({ fetch, parent }: PageLoadEvent)
{
    const projectPromise = (async () =>
    {
        const response = await fetch("/api/projects");
        return await response.json();
    })();

    const p = await parent();

    return {
        links: Navigation,
        streamed: {
            projects: projectPromise
        },
        meta: {
            ...p.meta,
            title: "ASTRELION's Projects",
            description: "ASTRELION's GitLab and GitHub projects",
        } satisfies PageMeta
    };
}
