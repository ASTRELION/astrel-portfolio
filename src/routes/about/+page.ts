import type { PageMeta } from "$lib/types";
import type { PageLoadEvent } from "./$types";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export async function load({ parent }: PageLoadEvent)
{
    const p = await parent();
    return {
        meta: {
            ...p.meta,
            title: "ASTRELION's About",
            description: "ASTRELION's about page",
        } satisfies PageMeta
    };
}
