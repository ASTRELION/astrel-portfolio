import { getDB } from "$lib/database.server";
import { kofi } from "$lib/db/schema";
import { json } from "@sveltejs/kit";

export async function GET(): Promise<Response>
{
    const db = await getDB();
    const rows = await db
        .select({
            name: kofi.name
        })
        .from(kofi)
        .execute();

    return json(Array.from(
        rows.reduce((p, c) =>
        {
            if (c.name !== null) p.add(c.name);
            return p;
        }, new Set<string>())
    ));
}
