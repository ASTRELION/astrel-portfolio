import { env } from "$env/dynamic/private";
import { getDB } from "$lib/database.server";
import { kofi } from "$lib/db/schema";
import { error, json, type RequestEvent } from "@sveltejs/kit";
import { sql } from "drizzle-orm";
import { isLeft } from "fp-ts/lib/Either";

import * as t from "io-ts";

// https://ko-fi.com/manage/webhooks
const KoFiPost = t.strict({
    verification_token: t.string,
    message_id: t.string,
    timestamp: t.string,
    type: t.union([
        t.literal("Donation"),
        t.literal("Subscription"),
        t.literal("Commission"),
        t.literal("Shop Order"),
    ]),
    is_public: t.boolean,
    from_name: t.string,
    message: t.union([
        t.string,
        t.null,
        t.undefined,
    ]),
    amount: t.string,
    url: t.string,
    email: t.string,
    currency: t.string,
    is_subscription_payment: t.boolean,
    is_first_subscription_payment: t.boolean,
    kofi_transaction_id: t.string,
    shop_items: t.union([
        t.array(t.strict({
            direct_link_code: t.string,
            variation_name: t.string,
            quantity: t.Int,
        })),
        t.null,
        t.undefined,
    ]),
    tier_name: t.union([
        t.string,
        t.null,
        t.undefined,
    ]),
    shipping: t.union([
        t.strict({
            full_name: t.string,
            street_address: t.string,
            city: t.string,
            state_or_province: t.string,
            postal_code: t.string,
            country: t.string,
            country_code: t.string,
            telephone: t.string,
        }),
        t.null,
        t.undefined,
    ]),
});

type KoFiData = t.TypeOf<typeof KoFiPost>;

export async function POST({ request }: RequestEvent): Promise<Response>
{
    const jsonData = (await request.formData()).get("data")?.toString();
    const data = KoFiPost.decode(JSON.parse(jsonData!));
    if (isLeft(data))
    {
        throw error(400, "Invalid input.");
    }

    const decoded: KoFiData = data.right;
    if (decoded.verification_token !== env.KOFI_WEBHOOK_TOKEN)
    {
        throw error(401, "Invalid token.");
    }

    const db = await getDB();
    const insert = db
        .insert(kofi)
        .values({
            name: decoded.from_name,
            message: decoded.message,
            type: decoded.type,
            public: decoded.is_public,
            isSubscriptionPayment: decoded.is_subscription_payment,
            isFirstSubscriptionPayment: decoded.is_first_subscription_payment,
            tier: decoded.tier_name,
            shopItems: decoded.shop_items
        });
    await insert.execute();
    return json({});
}

export async function GET(): Promise<Response>
{
    throw error(501, "This is a POST only endpoint.");
}
