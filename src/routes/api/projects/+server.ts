import { json } from "@sveltejs/kit";
import type { RequestEvent } from "./$types";
import featured from "$lib/featured";
import type { Project } from "$lib/types";
import { getRedisData, setRedisData } from "$lib/database.server";

const GITLAB_URL = "https://gitlab.com/api/v4/users/ASTRELION";
const GITHUB_URL = "https://api.github.com/users/ASTRELION";

export async function GET({ fetch, url }: RequestEvent): Promise<Response>
{
    const cached = await getRedisData(url);
    if (cached !== null)
    {
        return json(cached);
    }

    const requests = await Promise.all([
        getGitLabPages(fetch),
        getGitHubPage(fetch)
    ]);

    // get GitLab projects
    let gitlabProjects: Project[] = [];
    try
    {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const rawProjects: any[] = requests[0].body;
        const projects = rawProjects.map((p) =>
        {
            const feat = featured.find((f) => p.web_url.toLowerCase().includes(f.url.toLowerCase()));

            return {
                name: p.name,
                description: p.description,
                tags: p.tag_list,
                url: p.web_url,
                stars: p.star_count,
                forks: p.forks_count,
                isFork: false,
                createdAt: new Date(p.created_at),
                updatedAt: new Date(p.last_activity_at),
                image: p.avatar_url,
                featured: feat !== undefined ? featured.indexOf(feat) : null,
                altTitle: feat ? feat.title! : null,
                altDescription: feat ? feat.description! : null,
                altImage: feat ? feat.image! : null,
                platform: "gitlab",
            } satisfies Project;
        });
        gitlabProjects = projects;
    }
    catch (e)
    {
        console.log(`Error fetching GitLab projects: ${e}`);
    }

    // get GitHub projects
    let githubProjects: Project[] = [];
    try
    {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const rawProjects: any[] = requests[1].body;
        let projects = rawProjects.map((p) =>
        {
            const feat = featured.find((f) => p.html_url.toLowerCase().includes(f.url.toLowerCase()));

            return {
                name: p.name,
                description: p.description,
                tags: p.topics,
                url: p.html_url,
                stars: p.stargazers_count,
                forks: p.forks_count,
                createdAt: new Date(p.created_at),
                updatedAt: new Date(p.updated_at),
                isFork: p.fork,
                image: null,
                featured: feat !== undefined ? featured.indexOf(feat) : null,
                altTitle: feat ? feat.title! : null,
                altDescription: feat ? feat.description! : null,
                altImage: feat ? feat.image! : null,
                platform: "github",
            } satisfies Project;
        });
        projects = projects.filter((x) => x.name !== ".github");
        githubProjects = projects;
    }
    catch (e)
    {
        console.log(`Error fetching GitHub projects: ${e}`);
    }

    const projects = [...gitlabProjects, ...githubProjects];
    projects.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());

    setRedisData(url, projects);
    return json(projects);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getGitLabPages(fetch: any): Promise<{ body: any }>
{
    let page = await getGitLabPage("1", fetch);
    const body = [];
    body.push(...page.body);
    while (page.next)
    {
        page = await getGitLabPage(page.next!, fetch);
        body.push(...page.body);
    }
    return { body: body };
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getGitLabPage(i: string, fetch: any): Promise<{ body: any, next: any }>
{
    const apiResponse = await fetch(`${GITLAB_URL}/projects?page=${i}`);
    const next = apiResponse.headers.get("x-next-page");
    const body = await apiResponse.json();
    return { body: body, next: next };
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function getGitHubPage(fetch: any): Promise<{ body: any }>
{
    const apiResponse = await fetch(`${GITHUB_URL}/repos`);
    const body = await apiResponse.json();
    return { body: body };
}
