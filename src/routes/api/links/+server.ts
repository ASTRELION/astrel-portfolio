import { json } from "@sveltejs/kit";
import links from "$lib/links";

export async function GET(): Promise<Response>
{
    return json(links);
}
