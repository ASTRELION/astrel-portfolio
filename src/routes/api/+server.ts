import { json } from "@sveltejs/kit";

export async function GET(): Promise<Response>
{
    return json({
        links: "/api/links",
        projects: "/api/projects",
    });
}
