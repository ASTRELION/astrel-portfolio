// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
import type { PageMeta } from "$lib/types";

declare global
{
    namespace App
    {
        // interface Error {}
        // interface Locals {}
        interface PageData
        {
            meta?: PageMeta
        }
        // interface Platform {}
    }
}

export {};
