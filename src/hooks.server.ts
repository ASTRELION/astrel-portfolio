import { getDB, getRedis } from "$lib/database.server";

// initiate database and run migrations
getDB();
getRedis();
